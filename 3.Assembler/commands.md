# Assembler
### Creating assembly object output with:
```bash
gcc ../main.c -c -o 3.Assembler/main.o
gcc ../mathx.c -c -o 3.Assembler/mathx.o
```
### Reading disassemble from object file
```bash
objdump --disassemble main.o -M intel > main_disassemble
objdump --disassemble mathx.o -M intel > mathx_disassemble
```

### Reading symbol table from object file
```bash
objdump --syms main.o > main_symbols_table
objdump --syms mathx.o > mathx_symbols_table
```

### Look more in man objdump